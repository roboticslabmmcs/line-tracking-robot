#define MOTOR_1_PWM 3
#define MOTOR_2_PWM 5
#define MOTOR_1_1 11
#define MOTOR_1_2 10
#define MOTOR_2_1 9
#define MOTOR_2_2 8

#define IR_LEFT 6
#define IR_CENTER 4
#define IR_RIGHT 7

#define ST_LEFT 4
#define ST_RIGHT 1
#define ST_CENTER 2

#define LEFT_TURN 1
#define RIGHT_TURN 2

byte present = 0;
byte past = 0;
//int a[] = {1,2,1};
int i = 0;
int forward_speed = 2;
int turning_speed = 8;

byte get_IR ()
{
  byte stat = 0;

  stat += digitalRead(IR_LEFT) * ST_LEFT;
  stat += digitalRead(IR_CENTER) * ST_CENTER;
  stat += digitalRead(IR_RIGHT) * ST_RIGHT;

  return stat;
}

///////// Функции движения /////////
void Right_Motor_backward()
{
  digitalWrite(MOTOR_1_1, HIGH);
  digitalWrite(MOTOR_1_2, LOW);
}

void Right_Motor_forward()
{
  digitalWrite(MOTOR_1_1, LOW);
  digitalWrite(MOTOR_1_2, HIGH);
}

void Left_Motor_backward()
{
  digitalWrite(MOTOR_2_1, HIGH);
  digitalWrite(MOTOR_2_2, LOW);
}

void Left_Motor_forward()
{
  digitalWrite(MOTOR_2_1, LOW);
  digitalWrite(MOTOR_2_2, HIGH);
}

void Forward()
{
  Left_Motor_forward();
  Right_Motor_forward();
}

void Backward()
{
  Left_Motor_backward();
  Right_Motor_backward();
}

void Set_Left_speed(int spd_in_percent)
{
  int spd = 0;
  if (spd_in_percent == 0)
    analogWrite(MOTOR_2_PWM, 0);
  else
    analogWrite(MOTOR_2_PWM, (spd_in_percent * 1.3) + 121);
}

void Set_Right_speed(int spd_in_percent)
{
  int spd = 0;
  if (spd_in_percent == 0)
    analogWrite(MOTOR_1_PWM, 0);
  else
    analogWrite(MOTOR_1_PWM, (spd_in_percent * 1.3) + 125);
}

void Run() // вперёд
{
  Set_Left_speed(forward_speed);
  Set_Right_speed(forward_speed);
  //??????
  //delay(30);
  //Set_Left_speed(0);
  //Set_Right_speed(0);
  //delay(15);
  //??????
}


void Strong_Turn_Right()    //45 градусов вправо
{
  Set_Left_speed(turning_speed);
  Set_Right_speed(0);
  delay(200);
  Set_Right_speed(forward_speed);
  Set_Left_speed(forward_speed);
}

void Weak_Turn_Right()     //22 градусов вправо
{
  Set_Left_speed(turning_speed);
  Set_Right_speed(0);
  delay(100);
  Set_Right_speed(forward_speed);
}

void Strong_Turn_Left()    //45 градусов влево
{
  Set_Right_speed(turning_speed);
  Set_Left_speed(0);
  delay(200);
  Set_Left_speed(forward_speed);
  Set_Right_speed(forward_speed);
}

void Weak_Turn_Left()     //22 градусов влево
{
  Set_Right_speed(turning_speed);
  Set_Left_speed(0);
  delay(200);
  Set_Left_speed(forward_speed);
}

void Stop()
{
  Set_Right_speed(0);
  Set_Left_speed(0);
}

void Turn_90_Left()
{
  Left_Motor_backward();
  Right_Motor_forward();
  Set_Left_speed(turning_speed);
  Set_Right_speed(turning_speed);
  delay(400);
  Stop();
  Right_Motor_forward();
  Left_Motor_forward();
}

void Turn_90_Right()
{
  Left_Motor_forward();
  Right_Motor_backward();
  Set_Left_speed(turning_speed);
  Set_Right_speed(turning_speed);
  delay(400);
  Stop();
  Right_Motor_forward();
  Left_Motor_forward();
}

void Selector(int i)
{
  switch (i)
  {
    case LEFT_TURN:
      {
        Turn_90_Left();
        break;
      }
    case RIGHT_TURN:
      {
        Turn_90_Right();
        break;
      }
  }
}

//////// Функции инициализации ////////
void Initialize_Motors()
{
  pinMode(MOTOR_1_PWM, OUTPUT);
  pinMode(MOTOR_2_PWM, OUTPUT);
  pinMode(MOTOR_1_1, OUTPUT);
  
  pinMode(MOTOR_1_2, OUTPUT);
  pinMode(MOTOR_2_1, OUTPUT);
  pinMode(MOTOR_2_2, OUTPUT);
}

void Initialize_IR()
{
  pinMode(IR_LEFT, INPUT_PULLUP);
  pinMode(IR_CENTER, INPUT_PULLUP);
  pinMode(IR_RIGHT, INPUT_PULLUP);
}

void Line_tracking()
{
  //past = present;
  
  present = get_IR();
  switch (present)
  {
    case 0:       //  0/0/0
      {
        Forward();
        Run();
        break;
      }
    case 1:       //  0/0/1
      {
        Strong_Turn_Right();
        break;
      }
    case 2:       //  0/1/0
      {
        Forward();
        Run();
        break;
      }
    case 3:       //  0/1/1
      {
        Strong_Turn_Right();
        break;
      }
    case 4:      //  1/0/0
      {
        Strong_Turn_Left();
        break;
      }
    case 5:      //  1/0/1
      {
        Forward();
        Run();
        break;
      }
    case 6:      //  1/1/0
      {
        Strong_Turn_Left();
        break;
      }
    case 7:      //  1/1/1
      {
        Forward();
        Run();
        //Selector(a[i]);
        //i++;
        break;
      }
  }
}

void Initialize_COM(int baudrate)
{
  Serial.begin(baudrate);
}

///////// Главная часть программы ////////
void setup()
{
  Initialize_COM(9600);
  Initialize_Motors();
  Initialize_IR();
  Forward();
  Stop();
  delay(2000);          // Задержка на старт робота
  Set_Right_speed(forward_speed);
  Set_Left_speed(forward_speed);
}

void loop()
{
  Line_tracking();
}
